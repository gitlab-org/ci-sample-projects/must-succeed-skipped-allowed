# Scenario- Pipeline must succeed and skipping is allowed


### Screenshots from MRs


1. Pipeline underway

![Screen_Shot_2021-05-13_at_12.19.29_PM](/uploads/c2582d97e9761db0b9608f820bcac335/Screen_Shot_2021-05-13_at_12.19.29_PM.png) 

------

2. Pipeline passed

![Screen_Shot_2021-05-13_at_12.26.07_PM](/uploads/8b30e79839eb90f00a914be576efc9bd/Screen_Shot_2021-05-13_at_12.26.07_PM.png)

-------

3. Pipeline failed/canceled

![Screen_Shot_2021-05-13_at_12.29.57_PM](/uploads/8a544c721bd86adcd8edec20f5b116d0/Screen_Shot_2021-05-13_at_12.29.57_PM.png) 

-------

4. Pipeline skipped

![Screen_Shot_2021-05-13_at_12.50.43_PM](/uploads/cfb168ee56b697be22303ec83e9ec1d2/Screen_Shot_2021-05-13_at_12.50.43_PM.png)
